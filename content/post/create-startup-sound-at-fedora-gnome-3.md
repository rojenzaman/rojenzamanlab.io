+++
categories = ["other"]
date = 2020-11-30T21:00:00Z
subtitle = ""
tags = ["libcanberra", "fedora"]
title = "Create Startup Sound at Fedora 34 (GNOME 40)"

+++
First select an ogg sound for startup sound, an example here startup sound is [desktop-login.oga](/uploads/desktop-login.oga).

Put your sound to the **/usr/share/sounds/freedesktop/stereo/** directory as: `/usr/share/sounds/freedesktop/stereo/desktop-login.oga`

**~~/usr/share/sounds/freedesktop/stereo/libcanberra-login-sound.desktop~~** **/usr/share/gnome/autostart/libcanberra-login-sound.desktop**  file is required to play the sound at startup.

`libcanberra-login-sound.desktop`:

```bash
[Desktop Entry]
Type=Application
Name=GNOME Login Sound
Comment=Plays a sound whenever you log in
Exec=/usr/bin/canberra-gtk-play --id="desktop-login" --description="GNOME Login"
OnlyShowIn=GNOME
AutostartCondition=GSettings org.gnome.desktop.sound event-sounds
X-GNOME-Autostart-Phase=Application
X-GNOME-Provides=login-sound
```

Its do execute:

```bash
/usr/bin/canberra-gtk-play --id="desktop-login" --description="GNOME Login"
```

But sometimes it doesn't work properly. canberra-gtk-play says:

```text
Failed to play sound: File or data not found
```

Journalctl log says:

```bash
canberra-boot[4278]: Failed to play event sound: Not available
```

For solve this problem put this code to Exec line:

```bash
/usr/bin/canberra-gtk-play --file="/usr/share/sounds/freedesktop/stereo/desktop-login.oga" --description="GNOME Login"
```

What did we do here? We specified the Ogg file directly to the service. `--file=""`

After applied the libcanberra-login-sound.desktop file look like:

```bash
[Desktop Entry]
Type=Application
Name=GNOME Login Sound
Comment=Plays a sound whenever you log in
Exec=/usr/bin/canberra-gtk-play --file="/usr/share/sounds/freedesktop/stereo/desktop-login.oga" --description="GNOME Login"
AutostartCondition=GSettings org.gnome.desktop.sound event-sounds
X-GNOME-Autostart-Phase=Application
X-GNOME-Provides=login-sound
```

Now everything ok. Finaly, enable the **canberra-system-shutdown** service.

```bash
$  sudo systemctl enable canberra-system-shutdown.service
```

And go to reboot try it :)

or
```bash
$ /usr/bin/canberra-gtk-play --file="/usr/share/sounds/freedesktop/stereo/desktop-login.oga" --description="GNOME Login"
```