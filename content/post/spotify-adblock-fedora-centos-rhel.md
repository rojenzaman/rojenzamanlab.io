+++
categories = ["unix - gnu/linux", "other"]
date = 2020-11-01T05:00:00Z
subtitle = ""
tags = ["spotify-adblock-linux", "rhel", "centos", "adblock", "spotify", "fedora"]
title = "Spotify AdBlock -  Fedora 32-33/CentOS/RHEL"

+++
## New method available

Please go to: [Spotidy Adblock Fedora 34](/post/spotify-adblock-fedora-34/)

***

To do this you must have the spotify-client package or spotify installation method with lpf-spotify-client. Doesn't work with flatpak and snapd.

## Install Spotify

### Fedora 32 - With LPF

To install and enable **RPM Fusion** repository on the Fedora system use the following commands.

    $ sudo dnf install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm \
    https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm

Then install **Spotify** use the following commands.

> Note: This section currently does not work with fedora 33, if you want switch Fedora 33 Section.

    $ sudo dnf install lpf-spotify-client
    $ lpf  approve spotify-client
    $ sudo -u pkg-build lpf build spotify-client 
    $ sudo dnf install /var/lib/lpf/rpms/spotify-client/spotify-client-*.rpm

### Fedora 33 - With negativo17 Repo

Go to **/etc/yum.repos.d** directory and add this repo:

    # cat > fedora-spotify.repo <<EOF
    [fedora-spotify]
    name=negativo17 - Spotify
    baseurl=https://negativo17.org/repos/spotify/fedora-$releasever/$basearch/
    enabled=1
    skip_if_unavailable=1
    gpgkey=https://negativo17.org/repos/RPM-GPG-KEY-slaanesh
    gpgcheck=1
    enabled_metadata=1
    metadata_expire=6h
    type=rpm-md
    repo_gpgcheck=0
    
    [fedora-spotify-source]
    name=negativo17 - Spotify - Source
    baseurl=https://negativo17.org/repos/spotify/fedora-$releasever/SRPMS
    enabled=0
    skip_if_unavailable=1
    gpgkey=https://negativo17.org/repos/RPM-GPG-KEY-slaanesh
    gpgcheck=1
    enabled_metadata=1
    metadata_expire=6h
    type=rpm-md
    repo_gpgcheck=0
    EOF

Then type this command for install:

    $ sudo dnf install spotify-client

## Build spotify-adblock-linux

Prerequisites: git, wget, tar, make, gcc, libcurl-devel.

First install libcurl headers:

    $ sudo dnf install libcurl-devel

Then download sources and build them:

    $ git clone https://github.com/abba23/spotify-adblock-linux.git
    $ cd spotify-adblock-linux
    $ wget -O cef.tar.bz2 http://opensource.spotify.com/cefbuilds/cef_binary_80.0.8%2Bgf96cd1d%2Bchromium-80.0.3987.132_linux64_minimal.tar.bz2
    $ tar -xf cef.tar.bz2 --wildcards '*/include' --strip-components=1
    $ make

Finally install:

    $ sudo make install

### Usage

#### Command-line

    $ LD_PRELOAD=/usr/local/lib/spotify-adblock.so spotify

#### Desktop File

You can also integrate it with your desktop environment by creating a .desktop file (e.g. spotify-adblock.desktop) in **/usr/share/applications** . This lets you easily run it from an application launcher without opening a terminal.

    # cat > /usr/share/applications/spotify-adblock.desktop <<EOF
    [Desktop Entry]
    Type=Application
    Name=Spotify (adblock)
    GenericName=Music Player
    Icon=spotify
    TryExec=spotify
    Exec=env LD_PRELOAD=/usr/local/lib/spotify-adblock.so spotify %U
    Terminal=false
    MimeType=x-scheme-handler/spotify;
    Categories=Audio;Music;Player;AudioVideo;
    StartupWMClass=spotify
    EOF

### Uninstalling

    $ sudo make uninstall

## Screenshots

![](/uploads/screenshot-from-2020-10-26-15-39-02.png)

![](/uploads/screenshot-from-2020-11-01-15-00-50.png)

![](/uploads/screenshot-from-2020-11-01-15-07-28.png)