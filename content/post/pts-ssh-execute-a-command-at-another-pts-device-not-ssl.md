+++
categories = ["unix - gnu/linux", "other"]
date = 2020-10-03T21:00:00Z
subtitle = ""
tags = ["pts", "C"]
title = " PTS SSH - Execute a command at another PTS Device - Not SSL :) "

+++
## Execute a command at another PTS Device

**Usage:**

    bash-5.0# ./ptsSSH 
    Usage: ./ptsSSH [-n] DEVNAME COMMAND
    Usage: '-n' is an optional argument if you want to push a new line at the end of the text
    Usage: Will require 'sudo' to run if the executable is not setuid root

**Examples:**

    bash-5.0# tty 
    /dev/pts/0
    bash-5.0# ./ptsSSH -n /dev/pts/1 "echo hello world > /dev/pts/0"
    bash-5.0# hello world

    bash-5.0# ./ptsSSH -n /dev/pts/1 "ps -l | tail -3 > /dev/pts/0"
    bash-5.0# 0 S  1000  108238  107706  0  80   0 - 57056 -      pts/1    00:00:00 bash
    4 R  1000  108767  108238  0  80   0 - 54699 -      pts/1    00:00:00 ps
    0 S  1000  108768  108238  0  80   0 - 53833 -      pts/1    00:00:00 tail

    bash-5.0# ./ptsSSH -n /dev/pts/1 "whoami > /dev/pts/0"
    bash-5.0# user1

### C Source:

```c
/*https://stackoverflow.com/a/40190182/13849027*/

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <string.h>
#include <unistd.h>

void print_help(char *prog_name) {
        printf("Usage: %s [-n] DEVNAME COMMAND\n", prog_name);
        printf("Usage: '-n' is an optional argument if you want to push a new line at the end of the text\n");
        printf("Usage: Will require 'sudo' to run if the executable is not setuid root\n");
        exit(1);
}

int main (int argc, char *argv[]) {
    char *cmd, *nl = "\n";
    int i, fd;
    int devno, commandno, newline;
    int mem_len;
    devno = 1; commandno = 2; newline = 0;
    if (argc < 3) {
        print_help(argv[0]);
    }
    if (argc > 3 && argv[1][0] == '-' && argv[1][1] == 'n') {
        devno = 2; commandno = 3; newline=1;
    } else if (argc > 3 && argv[1][0] == '-' && argv[1][1] != 'n') {
        printf("Invalid Option\n");
        print_help(argv[0]);
    }
    fd = open(argv[devno],O_RDWR);
    if(fd == -1) {
        perror("open DEVICE");
        exit(1);
    }
    mem_len = 0;
    for (i = commandno; i < argc; i++) {
        mem_len += strlen(argv[i]) + 2;
        if (i > commandno) {
            cmd = (char *)realloc((void *)cmd, mem_len);
        } else { // i == commandno
            cmd = (char *)malloc(mem_len);
        }

        strcat(cmd, argv[i]);
        strcat(cmd, " ");
    }
  if (newline == 0)
        usleep(225000);
    for (i = 0; cmd[i]; i++)
        ioctl (fd, TIOCSTI, cmd+i);
    if (newline == 1)
        ioctl (fd, TIOCSTI, nl);
    close(fd);
    free((void *)cmd);
    exit (0);
}
```