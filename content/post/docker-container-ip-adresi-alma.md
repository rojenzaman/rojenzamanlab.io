+++
categories = ["docker"]
date = 2020-03-01T08:05:00Z
subtitle = ""
tags = ["ip", "container", "image"]
title = "docker container ip adresi alma"

+++
    docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' container_name

container_name kısmına container adı gelecek