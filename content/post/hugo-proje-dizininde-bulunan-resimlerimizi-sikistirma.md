+++
categories = ["hugo"]
date = 2020-05-27T01:00:00Z
subtitle = ""
tags = ["code"]
title = "Hugo proje dizininde bulunan resimlerimizi sıkıştırma"

+++
Hugo proje dizinine git ve çalıştır:

    find static/images/uploads/ \( -name '*.png' -o -name '*.jpg' -o -name '*.jpeg' \) -print0 | xargs -0 -P8 -n2 mogrify -strip -thumbnail '1000>' -format jpg

İstersen script olarakta indirip proje dizinine dahil ederek istediğin zaman kullanabilirsin

    $ wget https://gist.githubusercontent.com/rojenzaman/daf83e3dbf5784224b007d05516faa59/raw/c06c08b469d8860bfc3c91db57aa9e3a46315860/image-compress.sh
    $ chmod +x image-compress.sh
    $ ./image-compress.sh