+++
categories = ["script"]
date = 2020-09-28T21:00:00Z
subtitle = ""
tags = ["exif", "pdf", "exiftool"]
title = "How to create new exif tag for pdf files as using exiftool config file on GNU/Linux?"

+++
First you need to define your `XMP` tag (a complete [example](https://exiftool.org/config.html) here)
```bash
$ cat config.cfg
          
%Image::ExifTool::UserDefined = (
  'Image::ExifTool::XMP::pdfx' => {
     PdfSubTitle => {
         Writable => 'string',
     },
  },
);   
1; # end
```
Then with following command the tag and it's value will be added:
```bash
$ exiftool -config config.cfg -PdfSubTitle="Sub Title" test.pdf
```
Confirm:
```bash
$ exiftool -PdfSubTitle test.pdf
  Pdf Sub Title                   : Sub Title
 
$ exiftool exiftool test.pdf | grep 'Pdf Sub Title'
  Pdf Sub Title                   : Sub Title
```