---
title: docker tüm containerları ve imageları silme
subtitle: ''
date: 2019-11-05T21:00:00.000+00:00
tags:
- image
- container
categories:
- docker

---
    docker rm -vf $(docker ps -a -q)

kodunu terminalde çalıştırarak containerları silin ardından aşağıdaki komutlarıda yazarak imageları sil.

    docker rmi -f $(docker images -a -q)

hepsini çalıştırdıktan sonra kalkmış olacaktır.

kontrol etmek için ise:

    docker ps && docker images

yazıp çalıştır.