+++
categories = ["other"]
date = 2020-07-26T21:00:00Z
subtitle = ""
tags = ["dokuwiki"]
title = "Dokuwiki'de Türkçe URL'ler için gerekli ayar. (deaccent)"

+++
## 1. Yol

Dokuwiki'mizde konfigürasyon ayarları bölümüne gidip `deaccent` kısmını bulalım. Ardından `romanize`'yi işaretleyelim.

## 2. Yol

Dokuwiki'nin bulunduğu dizinde **conf/dokuwiki.php** dosyasında `$conf['deaccent'] = 1;` değerini **2** ile değiştirelim.