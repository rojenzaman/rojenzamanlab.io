---
title: shred ile bir dizindeki bütün dosyaları şifreleyerek silme
subtitle: ''
date: 2020-02-24T21:01:00+00:00
tags:
- unix
- linux
- shred
categories:
- privacy

---
    for i in *; do shred -zuvn 5 $i; done

kodunu terminalde sileceğin dosyaların bulunduğu dizinde çalıştır.
