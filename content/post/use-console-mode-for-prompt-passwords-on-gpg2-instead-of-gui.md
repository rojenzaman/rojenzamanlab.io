+++
categories = ["security"]
date = 2022-04-14T21:00:00Z
subtitle = ""
tags = ["gpg"]
title = "Use console-mode for prompt passwords on GPG2 instead of GUI"

+++
To change the pinentry permanently, append the following to your `~/.gnupg/gpg-agent.conf`:


```bash
pinentry-program /usr/bin/pinentry-tty
```

(In older versions which lack pinentry-tty, use pinentry-curses for a 'full-terminal' dialog window.)

Tell the GPG agent to reload configuration:

```bash
gpg-connect-agent reloadagent /bye
```