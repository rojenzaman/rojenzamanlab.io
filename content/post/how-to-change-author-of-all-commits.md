+++
categories = ["git"]
date = 2021-04-02T21:00:00Z
subtitle = ""
tags = []
title = " How to change author of all commits?"

+++
Just type:

```bash
git filter-branch -f --env-filter "GIT_AUTHOR_NAME='yourname'; GIT_AUTHOR_EMAIL='youremail@example.com'; GIT_COMMITTER_NAME='yourname'; GIT_COMMITTER_EMAIL='youremail@example.com';" HEAD;
```

check:

```bash
git log
```

then push:

```bash
git push --force origin master
```