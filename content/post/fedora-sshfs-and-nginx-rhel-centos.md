+++
categories = ["server"]
date = 2020-10-02T21:00:00Z
subtitle = ""
tags = ["sshfs"]
title = "Fedora: SSHFS and NGINX (RHEL, CentOS)"

+++
find what provides the sshfs command:

    ~# yum whatprovides sshfs
    fuse-sshfs-3.7.0-3.fc32.x86_64 : FUSE-Filesystem to access remote filesystems via SSH
    Repo        : @System
    Matched from:
    Provide    : sshfs = 3.7.0-3.fc32
    
    fuse-sshfs-3.7.0-3.fc32.x86_64 : FUSE-Filesystem to access remote filesystems via SSH
    Repo        : fedora
    Matched from:
    Provide    : sshfs = 3.7.0-3.fc32

then install the fuse-sshfs package.

    ~# dnf install fuse-sshfs
    Last metadata expiration check: 1:35:51 ago on Sat 03 Oct 2020 07:05:34 PM CEST.
    Dependencies resolved.
    =========================================================================================================
     Package                   Architecture         Version                       Repository            Size
    =========================================================================================================
    Installing:
     fuse-sshfs                x86_64               3.7.0-3.fc32                  fedora                62 k
    Installing dependencies:
     fuse-common               x86_64               3.9.1-1.fc32                  fedora               8.2 k
     fuse3                     x86_64               3.9.1-1.fc32                  fedora                55 k
     fuse3-libs                x86_64               3.9.1-1.fc32                  fedora                93 k
    
    Transaction Summary
    =========================================================================================================
    Install  4 Packages
    
    Total download size: 218 k
    Installed size: 528 k
    Is this ok [y/N]: y
    Downloading Packages:
    (1/4): fuse-common-3.9.1-1.fc32.x86_64.rpm                               109 kB/s | 8.2 kB     00:00    
    (2/4): fuse-sshfs-3.7.0-3.fc32.x86_64.rpm                                371 kB/s |  62 kB     00:00    
    (3/4): fuse3-3.9.1-1.fc32.x86_64.rpm                                     328 kB/s |  55 kB     00:00    
    (4/4): fuse3-libs-3.9.1-1.fc32.x86_64.rpm                                579 kB/s |  93 kB     00:00    
    ---------------------------------------------------------------------------------------------------------
    Total                                                                    222 kB/s | 218 kB     00:00     
    Running transaction check
    Transaction check succeeded.
    Running transaction test
    Transaction test succeeded.
    Running transaction
      Preparing        :                                                                                 1/1 
      Installing       : fuse3-libs-3.9.1-1.fc32.x86_64                                                  1/4 
      Installing       : fuse-common-3.9.1-1.fc32.x86_64                                                 2/4 
      Installing       : fuse3-3.9.1-1.fc32.x86_64                                                       3/4 
      Installing       : fuse-sshfs-3.7.0-3.fc32.x86_64                                                  4/4 
      Running scriptlet: fuse-sshfs-3.7.0-3.fc32.x86_64                                                  4/4 
      Verifying        : fuse-common-3.9.1-1.fc32.x86_64                                                 1/4 
      Verifying        : fuse-sshfs-3.7.0-3.fc32.x86_64                                                  2/4 
      Verifying        : fuse3-3.9.1-1.fc32.x86_64                                                       3/4 
      Verifying        : fuse3-libs-3.9.1-1.fc32.x86_64                                                  4/4 
    
    Installed:
      fuse-common-3.9.1-1.fc32.x86_64      fuse-sshfs-3.7.0-3.fc32.x86_64      fuse3-3.9.1-1.fc32.x86_64     
      fuse3-libs-3.9.1-1.fc32.x86_64      
    
    Complete!

and we must be create mount dir for serving in the web directory:

    ~# mkdir /usr/share/nginx/html/remountfs

then give the SElinux permission for the created dir:

    ~# chcon -Rt httpd_sys_content_t /usr/share/nginx/html/remountfs

give permissions for ssfs files to other users:

    cat /etc/fuse.conf 
    # mount_max = 1000
    # user_allow_other

Uncomment `the user_allow_other` line.

Finally we mount the remote location to local web location:

    sshfs server:/var/www/test.com /usr/share/nginx/html/remountfs

Go to [http://localhost/remountfs](http://localhost/remountfs) page. Remote directory of **/var/www/test.com** pointed at remountfs web directory.

If you see the permisson deined page, type this command:

    chown -R nginx:nginx /usr/share/nginx/html/remountfs


Everythings ok.