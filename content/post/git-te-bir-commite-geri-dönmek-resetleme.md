+++
categories = ["git"]
date = 2020-03-03T08:00:00Z
subtitle = ""
tags = []
title = "git'te bir commite geri dönmek (resetleme)"

+++
    git reset --hard COMMIT-SHA-ID
    git push --force origin master