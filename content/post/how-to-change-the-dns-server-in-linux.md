+++
categories = ["other"]
date = 2020-07-05T21:00:00Z
subtitle = ""
tags = ["dns"]
title = "How to change the DNS server in Linux?"

+++
open this file

    $ sudo nano /etc/resolv.conf

Remove the IP address created by NetworkManager and add the follow line (I am add the 1.1.1.1 address to here.)

    nameserver 1.1.1.1

Save and exit, then run follow command for NetworkManager does not override the file

    $ sudo chattr +i /etc/resolv.conf

If you want edit the file type `sudo chattr -i /etc/resolv.conf` and add your configuration, finally retype `chattr +i` command.