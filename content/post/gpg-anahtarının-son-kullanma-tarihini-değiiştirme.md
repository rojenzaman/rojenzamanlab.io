+++
categories = ["security", "privacy"]
date = 2020-03-24T14:00:00Z
subtitle = ""
tags = ["gpg"]
title = "gpg anahtarının son kullanma tarihini değiştirme"

+++
ilk önce gpg anahtarımızın ipsini öğrenelim

    $ gpg --list-keys
    pub   1024D/B989893B 2007-03-07 [expired: 2009-12-31]
    uid                  George Notaras <gnotaras@example.org>
    sub   4096g/320D81EE 2007-03-07 [expired: 2009-12-31]

anahtarı düzenleyelim

    $ gpg --edit-key B989893B

gpg kabuğuna girmiş olman lazım, her şeyden önce neyi düzenlediğini bilmek için **list** komutunu gir.

    gpg> list
    
    pub  1024D/B989893B  created: 2007-03-07  expired: 2009-12-31  usage: SCA
                         trust: ultimate       validity: ultimate
    sub  4096g/320D81EE  created: 2007-03-07  expired: 2009-12-31  usage: E
    [ ultimate] (1). George Notaras <gnotaras@example.org>

Varsayılan olarak, hiçbir alt anahtar (sub) seçilmez, yani birincil anahtar (pub) üzerinde çalışılır. Üzerinde çalışacağın sub anahtarı, tuş komutunu ve ardından seçmek istediğiniz sub anahtarın numarasını (dizinini) çağırarak seçmek mümkündür. Seçim iletilmezse, herhangi bir sub anahtarın seçimi kaldırılır ve birincil anahtar üzerinde çalışmış olursun. 

    gpg> key 0
    
    pub  1024D/B989893B  created: 2007-03-07  expired: 2009-12-31  usage: SCA
                         trust: ultimate       validity: ultimate
    sub  4096g/320D81EE  created: 2007-03-07  expired: 2009-12-31  usage: E
    [ ultimate] (1). George Notaras <gnotaras@example.org>

son kullanma tarihini belirlemek için **expire** komutunu kullanalım.

    gpg> expire
    Changing expiration time for the primary key.
    Please specify how long the key should be valid.
             0 = key does not expire
            = key expires in n days
          w = key expires in n weeks
          m = key expires in n months
          y = key expires in n years
    Key is valid for? (0) 2y
    Key expires at 10/28/12 03:51:07
    Is this correct? (y/N) y
    
    You need a passphrase to unlock the secret key for
    user: "George Notaras <gnotaras@example.org>"
    1024-bit DSA key, ID NNNNNNNN, created 2007-03-07
    
    
    pub  1024D/B989893B  created: 2007-03-07  expires: 2012-10-28  usage: SCA
                         trust: ultimate       validity: ultimate
    sub  4096g/320D81EE  created: 2007-03-07  expired: 2009-12-31  usage: E
    [ ultimate] (1). George Notaras <gnotaras@example.org>

bu adımlardan sonra belirlenmiş olacaktır. 

Şimdide sub anahtarının son kullanma tarihini belireleyelim. **key 1** komutunu yazarak sub anahtarı seçiyoruz.

    gpg> key 1
    
    pub  1024D/B989893B  created: 2007-03-07  expires: 2012-10-28  usage: SCA
                         trust: ultimate       validity: ultimate
    sub*  4096g/320D81EE  created: 2007-03-07  expired: 2009-12-31  usage: E
    [ ultimate] (1). George Notaras <gnotaras@example.org>

**expire** ile bunuda belirleyelim

    gpg> expire
    Changing expiration time for a subkey.
    Please specify how long the key should be valid.
             0 = key does not expire
            = key expires in n days
          w = key expires in n weeks
          m = key expires in n months
          y = key expires in n years
    Key is valid for? (0) 2y
    Key expires at 10/28/12 03:02:43
    Is this correct? (y/N) y
    
    You need a passphrase to unlock the secret key for
    user: "George Notaras <gnotaras@example.org>"
    1024-bit DSA key, ID NNNNNNNN, created 2007-03-07
    
    
    pub  1024D/B989893B  created: 2007-03-07  expires: 2012-10-28  usage: SCA
                         trust: ultimate       validity: ultimate
    sub* 4096g/320D81EE  created: 2007-03-07  expires: 2012-10-28  usage: E
    [ ultimate] (1). George Notaras <gnotaras@example.org>

Son kullanma tarihlerini değiştirmiş oldun kaydetmek için **save** komutunu yazıp çık.

    gpg> save

bu kadar :)