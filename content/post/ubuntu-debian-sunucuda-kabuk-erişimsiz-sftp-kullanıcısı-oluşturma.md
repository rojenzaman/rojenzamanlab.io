+++
categories = ["server", "security"]
date = 2020-03-01T07:00:00Z
subtitle = ""
tags = ["ssh", "sftp", "debian", "ubuntu"]
title = "ubuntu/debian sunucuda kabuk erişimsiz sftp kullanıcısı oluşturma"

+++
### 1. Kullanıcı oluştur

    sudo adduser --shell /bin/false sftpuser

bu komut ile kabuk erişimsiz kullanıcı oluşturuyoruz

### 2. SFTP dizini oluştur

    sudo mkdir -p /var/sftp/files

dizin sahipliğini sftp kullanıcısı olarak değiştir

    sudo chown sftpuser:sftpuser /var/sftp/files

kök dizini için gerekli sahipliği ayarla

    sudo chown root:root /var/sftp
    sudo chmod 755 /var/sftp

### 3. SSH'i SFTP için konfigüre et

    sudo nano /etc/ssh/sshd_config

ile sshd config dosyasını aç ve satırın sonuna şunları ekle

    Match User sftpuser
    	ForceCommand internal-sftp
    	PasswordAuthentication yes
    	ChrootDirectory /var/sftp
    	PermitTunnel no
    	AllowAgentForwarding no
    	AllowTcpForwarding no
    	X11Forwarding no
    

ayarları kaydet ve çık. Ardından ssh servisini yeniden başlat

    sudo systemctl restart ssh

### 4. SFTP bağlantını test et

    $ sftp sftpuser@localhost
    
    sftpuser@localhost's password: 
    Connected to localhost.
    sftp> 