+++
categories = ["script"]
date = 2020-05-27T02:03:00Z
subtitle = ""
tags = ["hosts.deny", "fail2ban", "bash"]
title = " fail2ban'da 5 defadan fazla banlanan ip adresi olursa onu /etc/hosts.deny dosyasına ekleyecek bir script "

+++
**_hosts.deny_** dosyazı bizim Unix-Benzeri sistemlerde ip adresinden gelebilecek istekleri ve paketleri engellememizi sağlar

örnek kullanım (yazacağımız bu kural belirttiğimiz ip adresinden gelen ssh isteklerini engellemektedir.

    sshd: 213.60.165.77
    sshd: 106.12.57.38
    sshd: 45.148.10.92

#### fail2ban Nedir?

Fail2ban, sunucunuzdaki log dosyalarını takip ederek çok sayıda hatalı login yapılmaya çalışılan servis için (_Brute Force_ saldırısıda olabilir) , fail2ban konfigurasyon yapılandırmasında belirlediğiniz kadar **_login_** denemesine izin verir , bu limit aşımı olduğunda Linux sunuculardaki _iptables firewall_’i kullanarak illegal işlem yapmaya çalışan IP adresi veya adresleri için iptables üzerinde otomatik kural oluşturur.

Dolayısıyla bu tür durumlarda karşılaşıldığında sistem yöneticisi olarak sizlerin her an sunucuya login olup logları analiz etmesine gerek kalmadan basit ama etkili bir güvenlik otomasyonu oluşturmanızı sağlar. Fail2ban iptables yerine alternatif olarak **_tcpwrapper_** kullanarak da **_hosts.deny_** dosyasına da belirli servis için belirli IP adresine engelleyebilir.

#### fail2ban'da 5 defadan fazla banlanan ip adresi olursa onu /etc/hosts.deny dosyasına ekleyerek engelleyecek bir script

<script src="https://gist.github.com/rojenzaman/a4120677e62ee2de8d3a5ad5bac1bc73.js"></script>