+++
categories = ["security"]
date = 2020-03-01T05:00:00Z
subtitle = ""
tags = ["ssh", "linux"]
title = "/var/log/auth.log dosyasında ssh ile sunucuya defalarca yapılan hatalı girişleri numaralandırarak sıralamak"

+++
    grep -o "[0-9]\+\.[0-9]\+\.[0-9]\+\.[0-9]\+" /var/log/auth.log | sort | uniq -c | sort -n

komutu ile defalarca denemede bulunan ip adreslerini sıralıyoruz.