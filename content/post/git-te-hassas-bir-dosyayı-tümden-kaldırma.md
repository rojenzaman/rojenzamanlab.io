+++
categories = ["git"]
date = 2020-03-03T08:58:00Z
subtitle = ""
tags = ["privacy"]
title = "git'te hassas bir dosyayı tümden kaldırma"

+++
projenin root dizinine gidip 

    git filter-branch --tree-filter 'rm -f <silinecek_dosya_yolu>' HEAD

diyerek dosyayı tüm commit geçmişlerinden kaldıralım. ardından **--force** seçeneği ile push edelim:

    git push origin --force --all