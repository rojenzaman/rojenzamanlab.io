+++
categories = ["other"]
date = 2020-07-08T21:00:00Z
subtitle = ""
tags = ["script", "bash", "password"]
title = "Şifre Oluşturucu (Basit bir Bash Script)"

+++
#### scripti \~/bin dizinine `gen_pass.sh` olarak kaydet

```bash
#!/bin/bash

if [ "$#" -lt 1 ]; then
    echo "`basename $0` <8-92>";
    exit 1;
fi

PASS=`date +%s | sha256sum | base64 | head -c $1`

echo "$PASS";
```

#### kullanım

```bash
$ gen_pass.sh <8-92>      
```

#### örnek

```bash
$ gen_pass.sh 32
MjljZGFhZmY5NzE5NDFmOWZhOTc4MjA3
```

Gist: [https://gist.github.com/rojenzaman/7fd6bf6bcbf7636f38209a87fbb50950](https://gist.github.com/rojenzaman/7fd6bf6bcbf7636f38209a87fbb50950 "https://gist.github.com/rojenzaman/7fd6bf6bcbf7636f38209a87fbb50950")