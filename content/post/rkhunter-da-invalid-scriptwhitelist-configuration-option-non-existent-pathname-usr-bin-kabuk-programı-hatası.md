+++
categories = ["security"]
date = 2020-03-29T15:00:00Z
subtitle = ""
tags = ["rkhunter"]
title = "rkhunter'da \"Invalid SCRIPTWHITELIST configuration option: Non-existent pathname: /usr/bin/kabuk-programı*\" hatası"

+++
sisteminde bu yol/program olmadığı için hatayı alıyorsun.

Yapman gereken tek şey **nano /etc/rkhunter.conf** ile ayar dosyasını aç

    SCRIPTWHITELIST=/bin/egrep
    SCRIPTWHITELIST=/bin/fgrep
    SCRIPTWHITELIST=/bin/which
    SCRIPTWHITELIST=/usr/bin/groups
    SCRIPTWHITELIST=/usr/bin/ldd
    #SCRIPTWHITELIST=/usr/bin/lwp-request
    SCRIPTWHITELIST=/usr/sbin/adduser
    #SCRIPTWHITELIST=/usr/sbin/prelink
    #SCRIPTWHITELIST=/usr/bin/unhide.rb

hata veren dosyayı bulup, yorum satırına al. Bu kadar :)