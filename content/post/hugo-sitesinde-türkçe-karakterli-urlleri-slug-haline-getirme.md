+++
categories = ["hugo"]
date = 2020-03-01T09:11:00Z
subtitle = ""
tags = ["slug", "theme"]
title = "hugo sitesinde türkçe karakterli urlleri slug haline getirme"

+++
## Hugo'da Türkçe Url Slug

### indirme ve kullanım

    cd ~
    git clone https://github.com/rojenzaman/turkish-url-slug-in-hugo.git

hugo dizinine git

    cd your-hugo-repo

ve dosyaları çağırmak için şunları yaz:

``` 
$ cp ~/turkish-url-slug-in-hugo/TurkishCharacterToUrl.java .
$ javac TurkishCharacterToUrl.java
$ cp TurkishCharacterToUrl.class content/posts
$ cp ~/turkish-url-slug-in-hugo/url-slug.sh .
```

son olarak kabuk scripti çalıştır

    ./url-slug.sh