+++
categories = ["script"]
date = 2020-05-11T21:00:00Z
subtitle = " A small script that downloads 400 free books opened by Springer."
tags = ["springer", "pdf", "archive"]
title = "Springers'in erişime açtığı 400 kitabı indiren ufak bir script"

+++
öncelikle scripti indirelim:

    $ git clone https://github.com/rojenzaman/download-springers-free-books.git
    $ cd download-springers-free-books

Geçtiğimiz aylarda springers 400 kitabı erişime açtı.
Erişime açılan kitaplar bir liste halinde bulunuyordu, haliyle hepsini indirebilmek için
urlleri takip edip indirmek gerekiyordu; hepsini indirme seçeneği yoktu.

Ben de bu  400 urllyi parse edip indiren bir script yazdım.
`download.sh` dosyasını kabuğunda çalıştırdığında hepsi bulunduğun dizine eklenmiş olacaktır.

pdf dosyalarini indirmek icin

    $ ./download.sh

yaklasik 400 kitap urlsi var. pdf idlerden indirildigi icin idsi sonradan degistirmisse site, indirelemiyor.

indirilemeyen kitap sayısı bir 10 kadardir. kontrol etmek icin:

    $ ./check-if-no-exist.sh

İyi arşivlemeler.

### kaynak kodu

#### download.sh

    #!/bin/bash
    
    #tor | set the torify or torsocks. by default none
    torproxy=""
    
    site="https://link.springer.com/content/pdf/10.1007/"
    
    while IFS= read -r url
    do
    
    cutted=`wget -qO- "$url" | perl -l -0777 -ne 'print $1 if /<title.*?>\s*(.*?)\s*<\/title/si'`
    title=${cutted:: -15}
    id=${url: -17}
    
    $torproxy wget -O "$title.pdf" $site$id.pdf
    [ -s "$title.pdf" ] || rm "$title.pdf"
    
    done < urlscheck-if-no-exist.sh

#### check-if-no-exist.sh

    #!/bin/bash
    
    #tor | set the torify or torsocks. by default none
    torproxy=""
    
    site="https://link.springer.com/content/pdf/10.1007/"
    
    while IFS= read -r url
    do
    
    
    cutted=`wget -qO- "$url" | perl -l -0777 -ne 'print $1 if /<title.*?>\s*(.*?)\s*<\/title/si'`
    title=${cutted:: -15}
    id=${url: -17}
    
    if $torproxy wget --spider $site$id.pdf 2>/dev/null; then
     echo "exist -" $id " is a available. | " $title
     else
     echo -e "\n" $title "\nno exist : " $site$id.pdf "\nat " $url "\n"
     fi
    
    
    done < urls
