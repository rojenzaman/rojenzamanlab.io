+++
categories = ["server"]
date = 2020-09-11T21:00:00Z
subtitle = ""
tags = ["dnf"]
title = "Save DNF Logs (Bash Script)"

+++
**usage:**

```bash
$ usage: ./save.sh [-u] [-s <app_name>]
-u      update log
-s      specific app log
```

download: [https://gist.github.com/rojenzaman/b6d9b6821a66097c300b11ba8f1c7bd4](https://gist.github.com/rojenzaman/b6d9b6821a66097c300b11ba8f1c7bd4)

**source code:**

```bash
#!/bin/bash

function guide() {
echo "usage: ./`basename $0` [-u] [-s <app_name>]"
echo "-u      update log"
echo "-s      specific app log"
}


function updateLog() {
fileName="$(date +%d-%m-%Y)_after_update_installed"
echo -e "write app log\nMissing [CTRL+D] for exit from stdin\n"
stdText=$(</dev/stdin)
echo "$stdText" >> $fileName
echo -e "  \n\nlog saved to $fileName"
}


function specific() {
fileName="$(date +%d-%m-%Y)_after_${appName}_installed"
echo -e "write app log\nMissing [CTRL+D] for exit from stdin\n"
stdText=$(</dev/stdin)
echo "$stdText" >> $fileName
echo -e "  \n\nlog saved to $fileName"
}


while getopts ":us:" opt; do
  case ${opt} in
    u )
      updateLog;
      ;;
    s )
      appName=${OPTARG}
      specific;
      ;;
    : )
      echo "Missing option argument for -$OPTARG <app_name>"
      ;;
  esac
done


if [ "$#" -lt 1 ]; then
    guide;
fi
```