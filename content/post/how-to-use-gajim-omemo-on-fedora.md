+++
categories = ["unix - gnu/linux", "privacy"]
date = 2023-01-22T21:00:00Z
subtitle = ""
tags = ["omemo", "gajim", "xmpp", "fedora"]
title = "How to use Gajim + OMEMO on Fedora?"

+++
Install **gajim** and **python3-qrcode**:

```bash
sudo dnf install -y gajim python3-qrcode
```

Make sure you have **python3-qrcode** installed as it is required to enable the OMEMO plugin.

On the Gajim tab click Plugins or press Ctrl+E to open Plugins:

![](/uploads/screenshot-from-2023-01-24-20-57-00.png)

And search for OMEMO plugin, then click to install:

![](/uploads/screenshot-from-2023-01-24-20-58-24.png)

After the installation is complete, activate the OMEMO plugin:

![](/uploads/screenshot-from-2023-01-24-20-58-48.png)

That's it!