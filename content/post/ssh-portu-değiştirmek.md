+++
categories = ["security"]
date = 2020-03-03T05:00:00Z
subtitle = ""
tags = ["ssh"]
title = "ssh portu değiştirmek"

+++
**/etc/ssh/sshd_config** dosyasına aşağıdaki satırı ekle:

    Port 9022

sonrasında ssh servisini yeniden başlat. 

    service ssh restart

Bu kadar.