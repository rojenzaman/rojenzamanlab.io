+++
categories = ["security"]
date = 2020-03-03T06:00:00Z
subtitle = ""
tags = ["ssh"]
title = "Ssh Brute Force Saldırıları ve Fail2Ban ile Önleme"

+++
> alıntı: [https://medium.com/@keraattin/ssh-brute-force-sald%C4%B1r%C4%B1lar%C4%B1-ve-fail2ban-ile-%C3%B6nleme-b96411569a70](https://medium.com/@keraattin/ssh-brute-force-sald%C4%B1r%C4%B1lar%C4%B1-ve-fail2ban-ile-%C3%B6nleme-b96411569a70 "alıntı")

Bu yazıda ssh brute force salırılarından, ssh erişimine açık olan bir sunucunun ssh brute force saldırılarına karşı fail2ban programı ile korunmasından bahsedeceğiz.

Uygulayacağımız senaryoda sunucu olarak Ubuntu 18.04 LTS ve client/saldırgan olarak Kali linux kullanacağız. Şimdi bir ssh brute force atağında neler oluyor, sunucu tarafında loglar nerede tutuluyor, loglarda neler görünüyor bir göz atalım.

Uygulacağımız senaryoda sunucunun ip adresi 10.0.2.4 iken saldırganın ip adresi 10.0.2.6 dır.

![](/uploads/1*fXS0GPguK0J5knOmJqr5uQ.png)

Ssh brute force saldırıs için **hydra** aracını kullanacağız.

![](/uploads/1*djNmKdiVjRKd5FXFWzDYBA.jpeg)

Şifre tahmin saldırısı için kali linux içerisinde rockyou.txt wordlist’ini kullanacağız. Dilerseniz başka bir wordlist kullanabilirsiniz burada örnek olarak rockyou.txt’yi kullanıyoruz. Rockyou.txt dosyası kali linuxda **/usr/share/wordlists/** dizini altında bulunur. Dosya burada .gz uzantılı şekilde bulunur wordlisti kullanabilmek için **gunzip rockyou.txt.gz** komutu ile arşivden çıkarmalıyız.

Kullanıcı adı için de bir liste edinebilir, kendiniz oluşturabilir veya sadece belirli bir kullanıcıya yönelik saldırı yapabilirsiniz.

**hydra -help** komutu ile örnek kullanımları görebiliriz.

![](/uploads/1*Vpzbn0zCkdRSLF_-OLyUKA.png)

Şimdi, **hydra -l root -P /usr/share/wordlists/rockyou.txt 10.0.2.4 ssh** komutunu çalıştırıp saldırıyı başlatalım.Bu örnekte biz sadece root kullanıcısına yönelik saldırı yapıyoruz.

![](/uploads/1*2cagAk95fARUVLIz5ITaAw.png)

Şimdi sunucumuzda logları inceleyelim. Sunucuda authentication ile ilgili loglar **/var/log/auth.log** dosyasında tutulur.

Sunucudaki auth.log dosyasının içeriğine baktığımızda şu şekilde görünecektir:

![](/uploads/1*rdiP6XxICoH03nTY2mbxWA.png)

Gördüğünüz gibi root kullanıcısıyla birçok başarısız deneme yapılmış. Bu saldırıların önüne geçmek için neler yapılmalı?

* Sunucuya erişecek olan ip adreslerinin sadece kendi ip adresiniz yapabilirsiniz. Fakat dinamik ip adresi kullanıyorsanız burada sorun yaşanabilir. Çıkış ip adresiniz değiştiği zaman sunucunuza erişimi kaybedebilirsiniz.
* ssh portunu varsayılandan farklı bir port yapabilirsiniz. Ssh portunu değiştirmek için **/etc/ssh/sshd_config** dosyasında **port 22** yazan yeri istediğiniz port numarası ile değiştirerek varsayılan ssh portunu değiştirebilirsiniz. Port numarasını değiştirdikten sonra **sudo systemctl restart sshd** komutu ile ssh servisini tekrar başlatmanız gerekebilir. Fakat burada da port farklı da olsa, servisler nmap taramasıyla keşfedilebilir. Nmap ile servis taraması yapmak için **nmap -sV 10.0.2.4 -p 1–65535** komutu kullanılabilir. Burada -p parametresi port numarası aralığı vermek için kullanılmıştır.

![](/uploads/1*842cYQZBJAy5cIwgyjKu-g.png)

* Fail2Ban gibi 3rd party programlar kullanılabilir.

# Fail2Ban Nedir?

Fail2ban belirli servislere ait log dosyalarını takip ederek başarısız login girişimlerini tespit eder ve bu denemeler belirli bir sayıya ulaştığı zaman ilgili aktivitenin kaynak IP’si için Iptables’a bir drop kuralı ekler.

# Fail2ban Nasıl Kurulur?

Debian tabanlı sistemlerde **sudo apt-get install fail2ban** komutu ile kurabilirsiniz.

![](/uploads/1*DN5wDhkg466ifwkBb3_sEA.png)

# Fail2Ban Konfigürasyonları

Fail2ban programının konfigürasyonları **/etc/fail2ban/jail.conf** dosyasında tutulur. Varsayılan ayarları değiştirmemek için **jain.conf** dosyasının içeriğini **jail.local** adında bir dosya oluşturup onun içine kopyalarsak daha sağlıklı olur. Yapacağımız değişklikleri **jail.local** dosyasında rahatça yapabiliriz. **sudo cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local** komutu ile kopyalama işlemini yapabilirsiniz. etc/fail2ban/ içerisinde jail.local dosyası varsa konfigürasyonlar bu dosyadan okunur. Şimdi ayarlarından bahsedelim.

![](/uploads/1*bJlHVS94eA5Dc9vXDoDoww.png)

**bantime** : Bir ip’nin ne kadar süre ile banlanacağını belirler.  
**findtime** : Kaç dakika içerisinde başarısız denemeler yapılırsa banlanacağını belirleyen süre.  
**maxretry** : findtime da belirtilen süre içersinide maksimum kaç defa başarısız giriş yapılabileceğini belirler.

Jail.local dosyasının içerisinde farklı servisler için de fail2ban çalıştırmak için ayarlar bulunur.

![](/uploads/1*M6N5mwdSdCQnldyQFdPrDA.png)

Görüldüğü gibi her servisin altına bir boşluk bırakılıyor. Eğer o servisi kullanmak istiyorsak altındaki boşluğa **enabled = true** yazmamız gerekiyor.

![](/uploads/1*viOzrAtRQ4hfbjwLEBqDLA.png)

Başlangıçta ssh hariç tüm servisler disabled olarak geliyor. Sadece ssh servisi varsayılan olarak açık geliyor.

Ayarları varsayılan olarak bıraktığımızda **sudo fail2ban-client status** kodu ile fail2ban programının durumuna bakarsak böyle göreceğiz:

![](/uploads/1*7bHi56Bqm0QOh6_rb5EWow.png)

Şimdi fail2ban kurulu ve servis çalışıyorken bir ssh brute force saldırısı yapıldığında neler olacağını görelim. 10.0.2.6 ip adresine sahip kali’den saldırıyı başlattık ve sunucumuzda **sudo fail2ban-client status sshd** komutu ile sshd servisinin durumuna bakalım:

![](/uploads/1*1TJd4IERhOFmNrrphWrupw.png)

Görüldüğü gibi 56 kez başarısız deneme yapılmış, şu an 1 ip adresi banlanmış, toplamda 2 ip adresi banlanmış ve şu an banlanmış olan ip adreslerinin listesi veriliyor. Ayrıca fail2ban logları **/var/log/fail2ban.log** dosyasında tutulur. Saldırı olduğunda banlanan ip adreslerini **cat /var/log/fail2ban.log | grep Ban** komutu ile bakalım.

![](/uploads/1*OLcZx8g8eDYPYCjtDHGMqg.png)

Fakat burada jail.local dosyasında belirtilen bantime süresi dolduktan sonra ip adresinin ban’ı kaldırılıyor. Süresiz olarak banlamak isterseniz jail.local dosyasına **bantime = -1** olarak girmeniz gerekir. Veya ek olarak bantime süresini sabit tutarsınız ve X defa banlanan ip adresini **/etc/hosts.deny** içerisine ekleyebilirsiniz.

Gün sonunda çalışacak ve o gün X(ben 5 seçtim) defadan fazla banlanan ip adresi olursa onu /etc/hosts.deny dosyasına ekleyecek bir script yazdım ve sizinle paylaşıyorum.

<script src="https://gist.github.com/rojenzaman/a4120677e62ee2de8d3a5ad5bac1bc73.js"></script>

Bu script’i crontab içerisine 23:59 da çalışacak şekilde koyarsanız o gün içinde 5 defadan fazla banlanan ip adreslerini hosts.deny dosyasına ekleyecektir.

Son olarak yararlı olduğunu düşündüğüm birkaç fail2ban komutu paylaşıyorum:

Ip Banlama => **sudo fail2ban-client set sshd banip 10.0.2.6  
**Ban Kaldırma => **sudo fail2ban-client set sshd unbanip 10.0.2.6**   
Fail2Ban Durumu => **sudo fail2ban-client status**  
Fail2Ban ssh servisi durumu => **sudo fail2ban-client status sshd**