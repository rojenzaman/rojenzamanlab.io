+++
categories = ["other"]
date = 2020-03-29T14:00:00Z
subtitle = ""
tags = ["raspbian"]
title = "rasbpian GNU/Linux işletim sisteminde kök kullanıcı şifresini, tüm kullanıcalar için şifreli yapma"

+++
bu komutla ilgili dosyayı açalım

    sudo nano /etc/sudoers.d/010_pi-nopasswd

tek bir satırla karşılaşacaksın

     pi ALL=(ALL) NOPASSWD: ALL

bunla değiştir:

     pi ALL=(ALL) PASSWD: ALL