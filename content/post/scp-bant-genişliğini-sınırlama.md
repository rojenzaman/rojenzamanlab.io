+++
categories = ["server"]
date = 2020-03-29T02:00:00Z
subtitle = ""
tags = ["ssh"]
title = "scp bant genişliğini sınırlama"

+++
"-l" parametresi kullanılacak bant genişliğini sınırlar. Çok sayıda dosyayı kopyalamak için bir komut dosyası girdin, ancak bant genişliğinin SCP işlemi tarafından aşırı tüketilmesini istemiyorsan yararlı olacaktır.

    root@kali ~/Documents $ scp -l 400 Label.pdf test@202.x.x.x:.
    
    test@202.x.x.x's password:
    Label.pdf 100% 3672KB 50.3KB/s 01:13