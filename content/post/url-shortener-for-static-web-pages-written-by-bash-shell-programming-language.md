+++
categories = ["script"]
date = 2020-05-25T21:00:00Z
subtitle = ""
tags = ["url", "bash"]
title = " URL Shortener For Static Web Pages (written by bash shell programming language) "

+++
#### install:

    $ wget https://gist.githubusercontent.com/rojenzaman/d4afdd096f035f53b696f6bd8e6d61c9/raw/0085912836e4c383b15a92216043c5ba43da151c/url-shortener.sh
    $ chmod +x url-shortener.sh

#### usage:

    $ ./url-shortener.sh 
    ./url-shortener.sh <write url here> <write your site directory here> [second] ["title"] ["html code or txt"]

#### source:

```bash
#!/bin/bash
if [ "$#" -lt 2 ]; then
    echo "./`basename $0` <write url here> <write your site directory here> [second] [\"title\"] [\"html code or txt\"]";
    exit 1;
fi

url=$1
directory=$2

if [ "$#" -gt 1 ]; then
    second=$3
    
      if [ -z "$second" ]; then
      second="0"
      fi
      
    title=$4  
    html=$5
    
fi



cd $directory;


random_url=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 4 | head -n 1);

while [ -f "$directory/$random_url" ];

    do 
        random_url=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 4 | head -n 1);
    done


mkdir $random_url && cd $random_url;



cat > index.html << EOF
<!DOCTYPE html>
<html>
 <head>
  <meta charset="utf-8">
  <meta http-equiv="refresh" content="$second; url=$url" />
  <title>$title</title>
 </head>
 
  <body>
  <h1>redirect to <a href="$url">$url</a></h1><br>
  $html
 </body>
 
</html>
EOF



if [ -z "$title" ]; then title="null"; fi
if [ -z "$html" ]; then html="null"; fi      

echo "url            = $url"
echo "site directory = $directory"
echo "sleep time     = $second"
echo "title          = $title"
echo "html code      = $html"
echo "-------------------------------------------"
echo "SHORT URL      = http://yourhostname/$random_url"
```