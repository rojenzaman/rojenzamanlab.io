+++
categories = ["server"]
date = 2021-02-13T21:00:00Z
subtitle = ""
tags = ["cloudflare"]
title = "Redirecting One Domain to Another Domain on Cloudflare"

+++
This tutorial covers redirecting one domain to another. For this, we will be redirecting `domainA.com` to `domainB.com`.

The following settings need to be configured under the domain you are redirecting **from**, in this example, this is `domainA.com`

**Step 1, The DNS Record:**  
The first thing you will need is a DNS record for `@`, `www` and any other subdomains you want to redirect, set to cloudflare. This can point to any IP address as the redirection page rule will execute first. I would recommend pointing them to `192.0.2.1` , a dummy IP.

![](/uploads/1550a1c35f73d34d694f347a5299806a6929b043.png)

![](/uploads/75fc146ad4551ecb3cc68213cfbcf1fb22c4fa60.png)

**Step 2, The Page Rule:**  
The second step is the page rule that will perform the redirect. You should add one like this:

![](/uploads/981e91c404121ac6a3237c14715cf5e5db9c8d05.png)

This page rule will redirect as per the following examples:

`domainA.com` > `https://domainB.com`

`www.domainA.com` > `https://domainB.com`

`domainA.com/page` > `https://domainB.com`

`www.domainA.com/page` > `https://domainB.com`

Should the domain you are redirecting to be served on `www`, you can add `www` to the URL at the bottom of the page rule.