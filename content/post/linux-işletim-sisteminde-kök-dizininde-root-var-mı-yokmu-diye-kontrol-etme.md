---
title: linux işletim sisteminde kök dizininde root var mı yokmu diye kontrol etme
subtitle: ''
date: 2019-11-05T21:00:00+00:00
tags:
- find
- root
- linux
categories:
- security


---
    find / -type d 2>&1 | grep "Permission denied"

kök kullanıcı olduktan sonra yukarıdaki kodu konsolda yazıp çalıştıralım ve ardından sonuçları inceleyelim.
