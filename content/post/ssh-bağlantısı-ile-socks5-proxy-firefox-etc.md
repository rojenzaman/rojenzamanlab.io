---
title: ssh ile socks5 proxy bağlantısı (firefox, etc)
subtitle: ''
date: 2019-11-04T21:00:00.000+00:00
tags:
- socks5
- firefox
- proxy
- ssh
categories:
- security
- privacy

---
öncelikle terminali açıp sunucuya bağlanıyoruz

    ssh -D 1080 -N -C kullaci_adi@ip_adresi

ps aux ile bağlanıp bağlanmadığını kontrol ediyoruz

    ps aux | grep ssh

bağlanmışsa şöyle bir sonuç verecek:

    rojen      3644  0.0  0.1  57204  4312 pts/1    S+   Kas04   0:00 ssh -D 1080 -N -C kullaci_adi@ip_adresi

Sonrasında tarayıcıya gidip sol üst köşede > tercihler > ağ ayarları seçeneğine tıklayıp resimdeki gibi alanları dolduruyoruz.

![](/uploads/screenshot.webp)

DNS sızıntısını önlemek için "SOCKS v5 kullanırken DNS'yi proxy et" seçeneğini etkinleştirmeyi unutmuyoruz,

tamama basıp internette gezinmeye devam ediyoruz, artık özgürsün!