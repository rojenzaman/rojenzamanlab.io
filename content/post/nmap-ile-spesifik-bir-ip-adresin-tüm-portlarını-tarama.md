+++
categories = ["privacy"]
date = 2020-03-01T19:06:00Z
subtitle = ""
tags = ["nmap"]
title = "nmap ile spesifik bir ip adresin tüm portlarını tarama"

+++
### TCP ağları için

    sudo nmap -sT -p- 192.168.1.1

### UDP ağları için

    sudo nmap -sU -p- 192.168.1.1