+++
categories = ["other"]
date = 2021-01-28T21:00:00Z
subtitle = ""
tags = ["vanity address", "onion", "tor"]
title = "mkp224o binary for RHEL x86-64"

+++
### Install libsodium library

#### For RHEL

```bash
dnf install libsodium
```

### Download and run

```bash
wget https://gist.github.com/rojenzaman/81ca03865681a0e2477f16493dfee5a4/raw/1a65af03c4be17f4020cf7ada7b74e69503911a4/mkp224o
chmod 750 mkp224o
./mkp224o
```

> mkp224o [v1.5.0](https://github.com/cathugger/mkp224o/releases/tag/v1.5.0)