+++
categories = ["git"]
date = 2020-10-19T08:59:00Z
subtitle = ""
tags = []
title = "Git Split Repo"

+++
	git reset --soft <sha>
	git stash push -m "to be separated"

\--works--

    git checkout -b <new-branch>
    git stash apply stash@{0}
    git add .
    git commit -m "new commit"  
    git push origin new-branch