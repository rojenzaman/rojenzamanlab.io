---
title: resim dosyasının içine veri gizlemek
subtitle: ''
date: 2020-02-24T21:01:00+00:00
tags:
- steghide
- linux
categories:
- privacy
- security

---
    apt-get install steghide

bu komut ile steghideyi indiriyoruz

    steghide embed -cf resim.jpeg -ef veri.txt

yazıyoruz. ardından parola ile korumamızı isteyecek, bir parola girip devam ediyoruz.

    steghide info resim.jpeg

yazınca şifreyi girip içindeki dosyayı görüyoruz

son olarak ;

    steghide extract -sf resim.jpeg

ile gizlediğimiz dosyayı çıkartıyoruz
