+++
categories = ["server", "privacy", "security"]
date = 2020-07-05T16:00:00Z
subtitle = ""
tags = ["tigervnc", "tls", "ssl", "vnc"]
title = "TLS/Şifreli Güvenli VNC Sunucusu Oluşturmak İçin Kolay Rehber"

+++
Bu rehberde, VNC sunucusunu kurmayı ve TLS şifrelemeli güvenli VNC sunucu oturumlarını öğreneceğiz.  
Bu yöntem CentOS 6 ve 7'de test edilmiştir, ancak diğer sürümlerde / işletim sistemlerinde de çalışmalıdır (RHEL, Scientific Linux, Fedora etc).

## VNC server kurulumu

VNC sunucusunu makinelerimize kurmadan önce çalışan bir GUI'miz olduğundan emin olun. Makinemize GUI kurulu değilse, aşağıdaki komutu yürüterek kurabiliriz,

    # yum groupinstall "GNOME Desktop"

Şimdi VNC sunucumuz olarak tigervnc'yi çalıştıracağız,

    # yum install tigervnc-server

VNC sunucusu kurulduktan sonra, sunucuya erişmek için yeni bir kullanıcı oluşturacağız,

    # useradd vncuser

aşağıdaki komutu kullanarak VNC'ye erişmek için bir parola atayın,

    # vncpasswd vncuser

Şimdi CentOS 6 ve 7'de yapılandırmada küçük bir değişiklik var, önce CentOS 6 yapılandırmasını ele alacağız,

### CentOS 6

Şimdi VNC yapılandırma dosyasını düzenlemeliyiz,

    # nano /etc/sysconfig/vncservers

aşağıdaki satırları ekleyin,

    VNCSERVERS="1:vncuser"
    VNCSERVERARGS[1]="-geometry 1024×768″

Dosyayı kaydedin ve çıkın. Ardından, değişiklikleri uygulamak için VNC servisini yeniden başlatın,

    # service vncserver restart

ön yüklemede aktif et,

    # chkconfig vncserver on

### CentOS 7

CentOS 7'de, `/etc/sysconfig/vncservers` dosyası `/lib/systemd/system/vncserver@.service`dosyasına değiştirildi. Bu yapılandırma dosyasını referans olarak kullanacağız, bu nedenle dosyanın bir kopyasını oluşturun,

    # cp /lib/systemd/system/vncserver@.service /etc/systemd/system/vncserver@:1.service

Sonrasında, oluşturulan kullanıcıyı içerecek şekilde dosyayı düzenleyeceğiz,

    # nano /etc/systemd/system/vncserver@:1.service

kullanıcıyı aşağıdaki 2 satırda düzenleyin,

    ExecStart=/sbin/runuser -l vncuser -c "/usr/bin/vncserver %i"
    PIDFile=/home/vncuser/.vnc/%H%i.pid

Kaydet ve çık. Sonra servisi yeniden başlat ve önyüklemede aktif et.

    # systemctl restart vncserver@:1.service
    # systemctl enable vncserver@:1.service

Artık VNC sunucumuz hazır ve VNC sunucusunun IP adresini kullanarak bir istemci makineden bağlanabiliyoruz. Ancak bunu yapmadan önce, TLS şifrelemesi ile bağlantılarımızı güvence altına alacağız.

## VNC oturumunun güvenliğini sağlama

VNC sunucu oturumunu güvenli hale getirmek için, önce şifreleme yöntemini VNC sunucu oturumlarını güvenli hale getirecek şekilde yapılandıracağız. TLS şifrelemesi kullanacağız, ancak SSL şifrelemesini de kullanabiliriz. VNC sunucusunda TLS şifrelemesini kullanmaya başlamak için aşağıdaki komutu yürütün,

    # vncserver -SecurityTypes=VeNCrypt,TLSVnc

VNC'ye erişmek için bir parola girmeniz istenecektir (başka bir kullanıcı kullanıyorsanız. yukarıda belirtileni kullanın)

![](/uploads/image1.png)

Şimdi sunucuya VNC görüntüleyiciyi kullanarak istemci makineden erişebiliriz, VNC görüntüleyiciyi güvenli bir bağlantıyla başlatmak için aşağıdaki komutu kullanabiliriz,

    # vncviewer -SecurityTypes=VeNCrypt,TLSVnc 192.168.1.45:1

burada, 192.168.1.45 VNC sunucunun IP adresi.

![](/uploads/image2.png)

Şifreyi girin, sunucuya uzaktan erişebilir ve TLS şifrelemesiyle de buna erişebiliriz.

> Kaynak: https://linuxtechlab.com/secure-vnc-server-tls-encryption/     https://archive.is/wip/xZqhG