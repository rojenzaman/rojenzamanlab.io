+++
categories = ["server", "unix - gnu/linux"]
date = 2022-04-14T21:00:00Z
subtitle = ""
tags = ["parallel", "wget"]
title = "Wget multi-thread with list"

+++
```bash
parallel -a websites.txt --jobs 10 wget
```