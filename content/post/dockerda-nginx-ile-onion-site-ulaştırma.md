+++
categories = ["server", "docker", "privacy"]
date = 2020-03-01T08:07:00Z
subtitle = ""
tags = ["onion", "nginx", "tor"]
title = "dockerda, nginx ile .onion site çalıştırma"

+++
    docker pull strm/tor-hiddenservice-nginx

kodu ile docker image'ı pull ediyoruz

    $ docker run -it --rm -v $(pwd)/web:/web strm/tor-hiddenservice-nginx generate ^strm
    [+] Generating the address with mask: ^strm
    [+] Found matching domain after 137072 tries: strmfyygjp5st54g.onion
    [+] Generating nginx configuration for site  strmfyygjp5st54g.onion
    [+] Creating www folder
    [+] Generating index.html template

komutu ile 'strn' ile başlayan .onion adresi yaratıyoruz

adresi oluşturduktan sonra aşağıdaki komut ile docker container'ı koşturuyoruz

    docker run -d --restart=always --name hiddensite \
           -v $(pwd)/web:/web strm/tor-hiddenservice-nginx

![](/uploads/print.png)

ve sonuç hazır :)