+++
categories = ["script"]
date = 2020-03-28T21:00:00Z
subtitle = ""
tags = ["url"]
title = "Static Url Shortener"

+++
URL Shorter For Static Web Pages (written by bash shell programming language)

### install:

```bash
git clone https://github.com/rojenzaman/static-url-shortener.git
cd static-url-shortener
```

### usage:

```bash
./url-shortener.sh <write url here> <write your site directory here> [second] ["title"] ["html code or txt"]
```

> GNU General Public License v3.0