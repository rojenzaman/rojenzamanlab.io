+++
categories = ["security", "server"]
date = 2020-07-05T15:00:00Z
subtitle = ""
tags = ["container", "podman", "docker"]
title = "Docker’ın yerini Podman mı alıyor? Podman Nedir ? Podman Kurulumu"

+++
Bu makalede Docker ile Podman ‘i karşılaştırıp ,artılarını ve eksilerini göstermeye çalışacağım.Öncelikle bir Docker kullanıcısı iseniz Docker’ın bir deamon aracılığıyla çalıştığını biliyorsunuz.Docker Deamon’ı ayakta değilse Container’ınızı ayağa kaldırmıyorsunuz.Altta Docker şemasını inceleyebilirsiniz.

![](/uploads/1-vrhtppnshs6ghl00igqryw.png)

Podman ise direk Linux kernel’da çalışan **_runC container runtime process_**’i ile bir deamon’a ihtiyaç duymadan çalışıyor.

![](/uploads/1-cqa7yo1rhmzhocbf1gapog.png)

Docker da çalıştırdığınız bütün komut setini burda da çalıştırabilirsiniz.Yani çok yabancı olmayacaksınız.

    docker ps/docker images
    podman ps/podman images

GitHub reposuna alttaki linkten ulaşabilirsiniz.

[https://github.com/containers/libpod](https://github.com/containers/libpod "https://github.com/containers/libpod")

Biraz karşılaştırmasını yapacak olursak ;

* Podman tarafında bir servis ayağa kaldırmakla uğraşmıyorsunuz.
* Komut setleri birbirine benziyor.
* Podman,Dockerdan farklı şekilde storage olarak “/var/lib/docker“ yerine /var/lib/containers” dizinini kullanıyor.(OCI standartı)
* Image olarak uyumlular.Aynı image’ı hem Docker üzerinde hemde Podman üzerinde çalıştırabilirsiniz.OCI standartının gerektirdiği şekilde.
* Podman en önemli 2. özelliği bana göre ,Container’ınızı Kubernetes ortamına taşımasına yardımcı olması.Makalenin devamında bu konuya değineceğim.
* Root yetkisi olmayan bir kullanıcı podman komutunu çağırabiliyor.Her kullanıcı altında “\~/.local/share/containers” dizini altında bir repo bulunuyor.Bu da aslında aynı sistemdeki birden fazla kullanıcı kendi container’larını oluşturabilir ve işi bittikten sonra public registry’e push edebiliyor demek.
* Podman, Private registry lerin yanında Docker hub ve Quay.io gibi repository lerden image çekebiliyor.

> OCI standartına biraz değinecek olursam; 2015 yılında Docker, Red Hat, CoreOS, SUSE, Google ve diğer linux container endüstrisindeki liderler “[**Open Container Initiative**](https://www.opencontainers.org)” adı altında bağımsız bir oluşum kurdular.Bu çatı altında container image ve container runtime standartlarını belirleyip [**container/image**](https://github.com/containers/image) ve [**container/storage**](https://github.com/containers/storage) github repolarında paylaşıma açtılar.

Bu kadar karşılaştırmadan sonra Podman Kurulumuna geçebiliriz\[Fedora'da yüklü olarak gelmektedir\]. Alttaki repoyu enable edip podman i yüklüyoruz.

    [root@ip-172-31-9-54 ec2-user]# subscription-manager register --username=USER --password=PASS --auto-attach
    Registering to: subscription.rhsm.redhat.com:443/subscription
    The system has been registered with ID: e8f1234f-4054-4d6d-a44a-5b0590e74478
    The registered system name is: ip-172-31-9-54.eu-west-1.compute.internal
    Installed Product Current Status:
    Product Name: Red Hat Enterprise Linux Server
    Status:       Subscribed
    [root@ip-172-31-9-54 ec2-user]# subscription-manager repos --enable=rhel-7-server-extras-rpms
    Repository 'rhel-7-server-extras-rpms' is enabled for this system.
    [root@ip-172-31-9-54 ec2-user]# yum install -y podman

Aşağıda bir image pull etme denemesini yapıyoruz.

    #Docker hub üzerinden httpd image ını indiriyorum
    [root@ip-172-31-21-108 ec2-user]# podman pull httpd
    Trying to pull registry.access.redhat.com/httpd:latest...Failed
    Trying to pull docker.io/httpd:latest...Getting image source signatures
    Copying blob sha256:6ae821421a7debccb4151f7a50dc8ec0317674429bec0f275402d697047a8e96
     21.46 MB / 21.46 MB [======================================================] 1s
    Writing manifest to image destination
    Storing signatures
    d3a13ec4a0f1157fb3502ec1248f2b1e7dbeeb6a2e21e0c1d3f43b7a494221ed
    [root@ip-172-31-21-108 ec2-user]# podman pull quay.io/bitnami/apache##Image ları kontrol ediyorum.
    [root@ip-172-31-21-108 ec2-user]# podman images
    REPOSITORY                TAG      IMAGE ID       CREATED        SIZE
    quay.io/bitnami/apache    latest   ab51d58954ae   11 hours ago   168 MB
    docker.io/library/httpd   latest   d3a13ec4a0f1   2 weeks ago    137 MB#Container ı ayağa kaldırıp komut çalıştırıyorum.
    [root@ip-172-31-21-108 ec2-user]# podman run -it httpd bash
    root@e0ad55ace147:/usr/local/apache2# df -h
    Filesystem      Size  Used Avail Use% Mounted on
    overlay          10G  1.9G  8.2G  19% /
    [root@ip-172-31-21-108 ec2-user]# podman run -it apache bash
    Welcome to the Bitnami apache container
    Subscribe to project updates by watching https://github.com/bitnami/bitnami-docker-apache
    Submit issues and feature requests at https://github.com/bitnami/bitnami-docker-apache/issues1001@9d6cbf7063cf:/opt/bitnami/apache/htdocs$ df -h
    Filesystem      Size  Used Avail Use% Mounted on
    overlay          10G  2.0G  8.0G  21% /

Bir küçük bilgi daha verecek olursam alttaki dosyaya istediğiniz Repository ‘i ekleyebilirsiniz.Default olarak redhat,docker,fedora,quay.io ve centos repository’leri aktif edilmiş olarak geliyor.

    vi /etc/containers/registries.conf[registries.search]
    registries = ['registry.access.redhat.com', 'docker.io', 'registry.fedoraproject.org', 'quay.io', 'registry.centos.org']

Artık docker ‘ı kaldırmaya hazırsınız :) Sisteminizde Deamon olmadan çalışan bir runtime bizi bekliyor.

    Podman’in bir diğer özelliği de oluşturduğunuz image ları çok kolay bir şekilde Kubernetes ortamına taşıyabiliyorsunuz.Aslında yukarda bahsettiğim gibi en beğendiğim ikinci özelliği bu oldu.

> ### podman generate kube

Örnek olarak bir uygulama ayağa kaldırıyorum.

    [root@ip-172-31-21-108 ec2-user]# podman run -dt -p 8000:80 --name prometheusdemo quay.io/prometheus/prometheus
    5a537260109ca704c875df5bb8180c685ec129bf72eb75b2b23663c93fdc35ae
    [root@ip-172-31-21-108 ec2-user]# podman ps
    CONTAINER ID  IMAGE                                 COMMAND               CREATED        STATUS            PORTS                 NAMES
    5a537260109c  quay.io/prometheus/prometheus:latest  /bin/prometheus -...  6 seconds ago  Up 6 seconds ago  0.0.0.0:8000->80/tcp  prometheusdemo
    [root@ip-172-31-21-108 ec2-user]# podman generate kube prometheusdemo > prometheus.yaml
    [root@ip-172-31-21-108 ec2-user]# cat prometheus.yaml 
    # Generation of Kubernetes YAML is still under development!
    #
    # Save the output of this file and use kubectl create -f to import
    # it into Kubernetes.
    #
    # Created with podman-0.12.1.2
    apiVersion: v1
    kind: Pod
    metadata:
      creationTimestamp: 2019-02-27T15:48:14Z
      labels:
        app: prometheusdemo
      name: prometheusdemo-libpod
    spec:
      containers:
      - command:
        - /bin/prometheus
        - --config.file=/etc/prometheus/prometheus.yml
        - --storage.tsdb.path=/prometheus
        - --web.console.libraries=/usr/share/prometheus/console_libraries
        - --web.console.templates=/usr/share/prometheus/consoles
    

Bu yaml dosyasını kullanarak direk Kubernetes ortamıma entegre edebiliyorum.

    kubectl create -f prometheus.yaml

Kaynak: [https://medium.com/devopsturkiye/docker%C4%B1n-yerini-podman-m%C4%B1-al%C4%B1yor-podman-nedir-podman-kurulumu-41b2b69a5ae7](https://medium.com/devopsturkiye/docker%C4%B1n-yerini-podman-m%C4%B1-al%C4%B1yor-podman-nedir-podman-kurulumu-41b2b69a5ae7 "https://medium.com/devopsturkiye/")