+++
categories = ["privacy"]
date = 2020-03-11T09:08:00Z
subtitle = ""
tags = ["tor"]
title = "debian sistemde obfs4 tor köprüsünü kullanma"

+++
**/etc/apt/sources.list** dosyasına aşağıdaki satırı ekle. böylece obfs4proxy deposu eklenmiş olacaktır.

    deb http://deb.torproject.org/torproject.org obfs4proxy main

ardından obfs4proxy'i indir 

    $ sudo apt-get update && sudo apt-get install obfs4proxy

indirdikten sonra **/etc/tor/torrc** dosyasını düzenle, aşağıdaki satırları ekle

    #Bridge config
    RunAsDaemon 1
    ORPort 9001
    BridgeRelay 1
    ServerTransportPlugin obfs4 exec /usr/bin/obfs4proxy
    ExtORPort auto

tüm işlemler bittikten sonra tor servisini yeniden başlat.

    sudo service tor restart

**obfs4proxy** çalışıyormu diye log dosyasını kontrol et

    $ sudo tail -F /var/log/tor/log

şöyle bir sonuç alman lazım:

    [notice] Registered server transport 'obfs4' at '[::]:46396'
    [notice] Tor has successfully opened a circuit. Looks like client functionality is working.
    [notice] Bootstrapped 100%: Done
    [notice] Now checking whether ORPort <redacted>:9001 is reachable... (this may take up to 20 minutes -- look for log messages indicating success)
    [notice] Self-testing indicates your ORPort is reachable from the outside. Excellent. Publishing server descriptor.