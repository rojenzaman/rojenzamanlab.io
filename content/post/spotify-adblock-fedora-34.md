+++
categories = ["other"]
date = 2021-06-17T21:00:00Z
subtitle = ""
tags = ["fedora 34", "adblock", "fedora", "spotify"]
title = "Spotify Adblock Fedora 35/36/37/rawhide"

+++
## Install Spotify

First install the spotify package from [negativo17](https://negativo17.org/spotify-client/)'s repository.

Enable negativo17 repo:

```bash
dnf config-manager --add-repo=https://negativo17.org/repos/fedora-spotify.repo
```

Then install:

```bash
dnf install spotify-client
```

## Build Adblock

Install the dependencies:

```bash
dnf install rust cargo
```

Clone and build:

```bash
git clone https://github.com/abba23/spotify-adblock.git
cd spotify-adblock
make
```

Install to system path:

```bash
sudo make install
```

## Usage - Important Last Step

Normally on other GNU/Linux distribution users add the `LD_PRELOAD` value to the desktop file, but this method didn't work on Fedora 34 (I really don't know why). So we will add `LD_PRELOAD` to spotify's executable: `/usr/bin/spotify`.

Add `LD_PRELOAD` value to `/usr/bin/spotify`:

```bash
export LD_PRELOAD=/usr/local/lib/spotify-adblock.so
```

Look like this:

```bash
#!/usr/bin/sh
# Wrapper script for Spotify.

# The spotify binary has a RUNPATH of its origin folder. It requires a few
# librares compiled with minimum options (no external dependencies).
# The FFMpeg library is loaded ONLY on the system path libraries, ignoring the
# RUNPATH.

# So remove the RUNPATH from the binary, put all the libraries in its private
# folder and make sure that only the spotify binary can access them.

export LD_LIBRARY_PATH="/usr/lib64/spotify-client${LD_LIBRARY_PATH:+:$LD_LIBRARY_PATH}"
export SCALE_FACTOR="$(/usr/lib64/spotify-client/get-scale-factor.py)"
#HERE
export LD_PRELOAD=/usr/local/lib/spotify-adblock.so

exec /usr/lib64/spotify-client/spotify --force-device-scale-factor=$SCALE_FACTOR "$@" &
exec /usr/lib64/spotify-client/set-dark-theme-variant.py &
```

That's all! Run it.

![](/uploads/spotify-ss.png)