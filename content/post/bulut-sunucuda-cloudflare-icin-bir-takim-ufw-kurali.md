+++
categories = ["privacy", "security"]
date = 2020-04-22T21:00:00Z
subtitle = ""
tags = ["ufw", "fail2ban", "cloudflare"]
title = "Bulut sunucuda Cloudflare için bir takım UFW kuralı"

+++
Eğer web sitemizi **Cloudflare**'da host ediyorsak; güvenlik için belirleyeceğimiz **UFW** kuralları ile sadece cloudflaredan gelen istekleri kabul edebilecek şekilde bulut sunucumuzu yapılandırabiliriz.

öncelikle ufw kuralı belirlemeden önce ssh portuna sonrasında bağlanabilmemiz için, ssh portunuz nerede bulunuyorsa onunlala ilgili izin kuralını belirtmemiz gerekir. (benimki 9022'de bulunduğu için  9022 yazdım eğer değiştirmemişsen 22 yazman lazım)

    # ufw allow 9022
    # ufw allow ssh

Böyle yazarak sunucunun sadece ssh isteklerine yanıt verebilir hale getirmiş olduk, eğer **ssh brute force** saldırıları konusunda korunmak için sıkı kurallar istiyorsan [fail2ban](https://www.fail2ban.org/wiki/index.php/Main_Page) kullanman gerekir.

Şimdi gelelim Cloudflare'in bize CDN hizmetini sağlayabilmesi için gerekli Cloudflare IP kurallarını ekleme kısmına.

Şu yazdığım scripti çalıştır: (dilersen bunu kaydet ve devamlı kullan update etmek için)

    #!/bin/bash
    cd /tmp
    curl https://www.cloudflare.com/ips-v4 > ips-v4
    curl https://www.cloudflare.com/ips-v6 > ips-v6
    for i in `<ips-v4`; do ufw allow from $i proto tcp to any port 80,443; done
    for i in `<ips-v6`; do ufw allow from $i proto tcp to any port 80,443; done

Bunları yaptıktan sonra artık güvenle sunucumuzu host edebiliriz.

fail2banı kurmak önemli olacaktır, şifren gerekiyorsa rakamlarla olmasın ssh anahtarıyla koru sunucunu; SSH portunu değiştirmeyide düşünebilirsin.

Diğer kullanışlı scriptler için:

[https://github.com/rojenzaman/wp-scripts/](https://github.com/rojenzaman/wp-scripts/ "https://github.com/rojenzaman/wp-scripts/")