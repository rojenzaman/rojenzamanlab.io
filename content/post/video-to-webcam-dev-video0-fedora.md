+++
categories = ["other", "unix - gnu/linux"]
date = 2020-10-05T21:00:00Z
subtitle = ""
tags = ["video0", "webcam"]
title = "Video to Webcam (/dev/video0) - Fedora"

+++
First enable **sentry/v4l2loopback** repo for install the `v4l2loopback`

    dnf copr enable sentry/v4l2loopback

then install v4l2loopback:

    dnf install v4l2loopback

then run `modprobe v4l2loopback` for creating video device.

finally find your mp4 file and type this command for manipulating the webcam:

    ffmpeg -re -i input.mp4 -map 0:v -f v4l2 /dev/video0

**Screenshot:**

![](/uploads/dev-video.png)