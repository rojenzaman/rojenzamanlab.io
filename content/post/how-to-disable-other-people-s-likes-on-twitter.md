+++
categories = ["other"]
date = 2020-12-04T08:00:00Z
subtitle = ""
tags = ["twitter"]
title = "How to disable other people's likes on Twitter?"

+++
Go to muted words at Twitter. page link:

[https://twitter.com/settings/muted_keywords](https://twitter.com/settings/muted_keywords "https://twitter.com/settings/muted_keywords")

And paste those:

* `ActivityTweet`
* `RankedOrganicTweet`
* `suggest_activity_tweet`
* `suggested_recycled_tweet_inline`
* `suggested_grouped_tweet_hashtag`
* `suggest_pyle_tweet`
* `suggested_rank_organic_tweet`
* `suggest_ranked_timeline_tweet`
* `suggest_recycled_tweet`
* `suggest_sc_tweet`
* `suggest_recap`
* `suggest_who_to_follow`
* `generic_activity_Highlights`

That's it, go to try.

![](/uploads/banksy-twitter-fight1.png)